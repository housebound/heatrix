package com.werware.heatrix;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.Spinner;


import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Button showHeatMap_btn;
    FirebaseDatabase database;
    DatabaseReference myRef;
    public ArrayList<String> datesAvailable;
    public ArrayList<DataModel> availableDatesTime = new ArrayList<>();
    Spinner dateSpinner;
    Spinner timeSpinner;
    ArrayList<String> availableDates;
    ArrayList<String> availableTimeforSpinner;
    String selectedDate = "";
    String selectedTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage("Downloading Data Please wait.. ");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        progress.setIndeterminate(true);
        //Initializing firebase handles for retrieving data
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users/aashirShop-6515");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    myRef = database.getReference("users/aashirShop-6515/" + dataSnapshot1.getKey().toString());
                    myRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            datesAvailable = new ArrayList<String>();
                            for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()) {
                                if (datesAvailable.contains(dataSnapshot2.getKey().toString())) {

                                } else
                                    datesAvailable.add(dataSnapshot2.getKey().toString());

                            }

                            DataModel model = new DataModel();

                            model.setDate(dataSnapshot1.getKey().toString());
                            model.setAvailableTime(datesAvailable);
                            availableDatesTime.add(model);
                            Log.v("DataRecieved", String.valueOf(availableDatesTime.size()));

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            progress.dismiss();
                        }
                    });


                    Log.v("dataRecievedkey", dataSnapshot1.getKey().toString());
                }
                progress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progress.dismiss();
            }

        });

        showHeatMap_btn = (Button) findViewById(R.id.show_heatMap_btn);
        showHeatMap_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(HomeActivity.this);
                dialog.setContentView(R.layout.dialog_spinners);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
                dateSpinner = (Spinner) dialog.findViewById(R.id.spinner_date);
                timeSpinner = (Spinner) dialog.findViewById(R.id.spinner_time);
                timeSpinner.setOnItemSelectedListener(HomeActivity.this);
                dateSpinner.setOnItemSelectedListener(HomeActivity.this);
                availableDates = new ArrayList<String>();
                for (DataModel dataModel : availableDatesTime) {
                    availableDates.add(dataModel.getDate());
                }

                ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),
                        android.R.layout.simple_spinner_item,
                        availableDates);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                dateSpinner.setAdapter(adapter);

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                Button show = (Button) dialog.findViewById(R.id.display_btn);
                show.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!selectedTime.isEmpty() && !selectedDate.isEmpty()) {
                            Intent intent = new Intent(HomeActivity.this, HeatMapActivity.class);
                            intent.putExtra("date", selectedDate);
                            intent.putExtra("time", selectedTime);
                            startActivity(intent);
                        }

                    }
                });
                dialog.show();
            }


        });

        GraphView graph = (GraphView) findViewById(R.id.graph);
        graph.getGridLabelRenderer().setHorizontalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setVerticalLabelsColor(Color.WHITE);
        graph.getGridLabelRenderer().setGridColor(Color.WHITE);
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[]{
                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3),
                new DataPoint(3, 2),
                new DataPoint(4, 6)
        });


        series.setAnimated(true);

        graph.addSeries(series);

        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX).toString();
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX);
                }
            }
        });

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"});
        staticLabelsFormatter.setVerticalLabels(new String[]{"30", "60", "90", "120", "150"});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

        Log.v("DataRecieved", String.valueOf(availableDatesTime.size()));

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.spinner_date:
                for (DataModel model : availableDatesTime) {
                    if (model.getDate().contains(availableDates.get(i))) {
                        availableTimeforSpinner = model.getAvailableTime();
                        selectedDate = availableDates.get(i);
                        Log.v("SpinnerAdapter", String.valueOf(availableTimeforSpinner.size()));
                    }
                }
                ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),
                        android.R.layout.simple_spinner_item,
                        availableTimeforSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                timeSpinner.setAdapter(adapter);
                break;

            case R.id.spinner_time:
                selectedTime = availableTimeforSpinner.get(i);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}


