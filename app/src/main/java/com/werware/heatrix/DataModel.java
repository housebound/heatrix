package com.werware.heatrix;

import java.util.ArrayList;

/**
 * Created by Xorrow on 05-Jun-17.
 */

public class DataModel {
    String date;
    ArrayList<String> availableTime = new ArrayList<>();

    public DataModel(String date, ArrayList<String> availableTime) {
        this.date = date;
        this.availableTime = availableTime;
    }

    public DataModel() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<String> getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(ArrayList<String> availableTime) {
        this.availableTime = availableTime;
    }
}
