package com.werware.heatrix;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;


public class HeatMapActivity extends AppCompatActivity {
    ViewPager viewPager;
    ViewPagerAdapter myCustomPagerAdapter;
    FirebaseDatabase database;
    DatabaseReference myRef;
    List<Bitmap> imgList = new ArrayList<Bitmap>();
    int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heat_map);
        String date = "";
        String time = "";
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            Log.v("DataFromIntent", "Empty");
        } else {
            date = extras.getString("date");
            time = extras.getString("time");


        }
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("users/aashirShop-6515/" + date + "/" + time);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Log.v("DataFromFirebase", dataSnapshot1.getValue().toString());
                    imgList.add(decodeBase64(dataSnapshot1.getValue().toString()));


                }
                viewPager = (ViewPager) findViewById(R.id.viewPager);
                myCustomPagerAdapter = new ViewPagerAdapter(HeatMapActivity.this, imgList);
                viewPager.setAdapter(myCustomPagerAdapter);
                CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
                indicator.setViewPager(viewPager);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        ImageView back = (ImageView) findViewById(R.id.back_heatmap);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HeatMapActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

        // TODO Take images from firebase and store here


        //View Pager Stuff


        // TODO Take these 3 values from firebase instead of initializing to zero

        int customers_int = 35;
        int waiting_time_int = 20;
        int percentage_int = 50;

        TextView no_of_custs = (TextView) findViewById(R.id.no_of_customers_txtView);
        no_of_custs.setText(String.valueOf(customers_int));

        TextView waiting_time = (TextView) findViewById(R.id.avg_waiting_time_txtView);
        waiting_time.setText(String.valueOf(waiting_time_int));

        TextView percentage = (TextView) findViewById(R.id.percentage_inc_txtView);
        percentage.setText(String.valueOf(percentage_int));


        RoundCornerProgressBar customer_bar = (RoundCornerProgressBar) findViewById(R.id.customers_bar);
        customer_bar.setProgressColor(Color.parseColor("#81C784"));
        customer_bar.setProgressBackgroundColor(Color.parseColor("#424242"));
        customer_bar.setMax(100);
        customer_bar.setProgress(customers_int);

        RoundCornerProgressBar time_bar = (RoundCornerProgressBar) findViewById(R.id.waiting_time_bar);
        time_bar.setProgressColor(Color.parseColor("#F06292"));
        time_bar.setProgressBackgroundColor(Color.parseColor("#424242"));
        time_bar.setMax(100);
        time_bar.setProgress(waiting_time_int);

        RoundCornerProgressBar perc_bar = (RoundCornerProgressBar) findViewById(R.id.percentage_bar);
        perc_bar.setProgressColor(Color.parseColor("#4FC3F7"));
        perc_bar.setProgressBackgroundColor(Color.parseColor("#424242"));
        perc_bar.setMax(100);
        perc_bar.setProgress(percentage_int);


    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
}
